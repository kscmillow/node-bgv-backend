var express = require('express');
var router = express.Router();
const { validationResult, body, param } = require("express-validator");
let { errorResponse } = require("../utils/response");
let { login, create, updateStatus, createRole, roleById } = require("../controllers/admin/auth");
let { createCompany, companyList, companyById } = require("../controllers/admin/companies");
let { adminList, createCompanyUser, companyUserList } = require("../controllers/admin/users");


//admin crud apis
router.post('/login', [
  body("email", 'A valid email is required').optional().isEmail(),
  body("username", 'A valid username is required').optional(),
  body("password", 'A valid password is required').exists(),
], async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return errorResponse(res, errors)
  }
  return login(req, res);
});

router.post('/create', [
  body("email", 'A valid email is required').exists().isEmail(),
  body("username", 'A valid username is required').exists(),
  body("password", 'A valid password is required').exists(),
  body("name", 'A valid name is required').exists(),
  body("role_id", 'A valid role id is required').exists(),
  body("status", 'A valid status is required').exists().isIn(["active", "inactive"])
], async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return errorResponse(res, errors)
  }
  return create(req, res);
})

router.get('/list', async (req, res, next) => {
  return adminList(req, res)
})

router.put('/update', [
  body("id", 'A valid id is required').exists(),
  body("status", 'A valid status is required').exists().isIn(["active", "inactive"])
], async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return errorResponse(res, errors)
  }
  return updateStatus(req, res);
})


router.post('/role/create', [
  body("name", 'A valid name is required').exists(),
], async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return errorResponse(res, errors)
  }
  return createRole(req, res);
})

router.get('/role/:id', [
  param("id", "A valid id is required").exists()
], async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return errorResponse(res, errors)
  }
  return roleById(req, res);
})


//admin dashboard apis

router.get('/dashboard/candidates', async (req, res, next) => {

})

router.get('/dashboard/candidates/verified', async (req, res, next) => {

})

router.get('/dashboard/candidates/wip', async (req, res, next) => {

})


router.get('/dashboard/candidates/rejected', async (req, res, next) => {

})

router.get('/dashboard/candidates/search', async (req, res, next) => {

})





//admin company apis 
router.post('/company/create', [
  body("name", 'A valid name is required').exists(),
  body("status", 'A valid status is required').exists().isIn(["active", "inactive"])
], async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return errorResponse(res, errors)
  }
  return createCompany(req, res);
})

router.get('/company/:id/candidates', async (req, res, next) => {

})

router.get('/company/:id', async (req, res, next) => {
  return companyById(req, res);
})

router.get('/company/list', async (req, res, next) => {
  console.log("req body")
  return companyList(req, res);
})

router.post('/company/users/create', [
  body("email", 'A valid email is required').exists().isEmail(),
  body("username", 'A valid username is required').exists(),
  body("password", 'A valid password is required').exists(),
  body("name", 'A valid name is required').exists(),
  body("role_id", 'A valid role id is required').exists(),
  body("company_id", 'A valid company id is required').exists(),
  body("status", 'A valid status is required').exists().isIn(["active", "inactive"])
], async (req, res, next) => {
  console.log("req body")
  return createCompanyUser(req, res);
})

router.get('/company/users/:id', [
  param("id", 'A valid id is required').exists().isEmail(),
], async (req, res, next) => {
  console.log("req body")
  return companyUserList(req, res);
})

router.get('/company/:id/users', [
  param("id", 'A valid id is required').exists().isEmail(),
], async (req, res, next) => {
  console.log("req body")
  return companyUserList(req, res);
})


//admin candidate apis



module.exports = router;
