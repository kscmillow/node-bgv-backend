var express = require('express');
var router = express.Router();

let admin = require('./admin');
let company = require('./company');
let candidate = require('./candidate');
let agent = require("./agent");


router.use('/admin', admin);
router.use('/company', company);
router.use('/candidate', candidate);
router.use('/agent', agent);


module.exports = router;
