const mongoose = require("mongoose")
const uri = 'mongodb://' + process.env.DB_HOSTNAME +":"+ process.env.DB_PORT + "/" + process.env.DB_NAME;
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
console.log("connecting to...")
console.log(uri);

