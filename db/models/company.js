const mongoose = require("mongoose");
let schema = new mongoose.Schema({
    name: String,
    status: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('company', schema);