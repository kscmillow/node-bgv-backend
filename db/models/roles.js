let mongoose = require("mongoose");
let schema = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
    },
    permissions: [],
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Roles', schema);