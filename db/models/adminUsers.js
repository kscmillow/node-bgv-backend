const mongoose = require("mongoose");
let schema = new mongoose.Schema({
    name: String,
    username: {
        type: String,
        unique: true,
        index: true
    },
    email: {
        type: String,
        unique: true
    },
    password:{
        type:String
    },
    status: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    },
    role_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'roles'
    }
});

module.exports = mongoose.model('AdminUser', schema);