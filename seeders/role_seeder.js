let dotenv = require("dotenv");
dotenv.config({ path: "../.env" });
require("../db/connection");

let roles = [
    {
        name: "superadmin",
        permissions: [""]
    },
    {
        name: "sales",
        permissions: [""]
    },
]
let RoleModel = require("../db/models/roles");

RoleModel.insertMany(roles).then((r)=>{
    console.log(r)
}).catch((error)=>{
    console.error(error)
})