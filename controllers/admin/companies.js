let { errorResponse, successResponse } = require("../../utils/response");
const mongoose = require("mongoose")
let Company = require("../../db/models/company");



let createCompany = async (req, res) => {
    let { name, status } = req.body;
    try {
        let company = await Company.create({
            name: name,
            status: status
        })
        return successResponse(res, company)
    } catch (error) {
        return errorResponse(res, error)
    }
}
let companyList = async (req, res) => {
    console.log("company list")
    try {
        let companies = await Company.find({})
        return successResponse(res, companies)
    } catch (error) {
        return errorResponse(res, error)
    }
}

let companyById = async (req, res) => {

    let { id } = req.params;
    console.log(req.params)
    try {
        let company;
        if (id === "list") {
            company = await Company.find({});
            return successResponse(res, company)
        }
        company = await Company.findOne({
            _id: id
        });
        return successResponse(res, company)
    } catch (error) {
        console.error(error)
        return errorResponse(res, error)
    }
}

module.exports = {
    createCompany,
    companyList,
    companyById
}