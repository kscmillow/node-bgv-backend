let { generateToken } = require("../../utils/token");
let { generateHash, compareHash } = require("../../utils/bcrypt");
let { errorResponse, successResponse } = require("../../utils/response");
const mongoose = require("mongoose")
let AdminUsers = require("../../db/models/adminUsers");
let Roles = require("../../db/models/roles");

let login = async (req, res) => {
    let { email, username, password } = req.body;
    let userQuery = []
    if (!email && !username) {
        return errorResponse(res, {
            message: "username or email not found"
        })
    }
    if (email) {
        userQuery.push({
            email: email
        })
    }
    if (username) {
        userQuery.push({
            username: username
        })
    }
    try {
        let getUser = await AdminUsers.aggregate([
            {
                "$match": {
                    "$or": userQuery
                }
            },
            {
                "$lookup": {
                    "from": "roles",
                    "localField": "role_id",
                    "foreignField": "_id",
                    "as": "role"
                }
            }
        ])


        if (getUser && getUser.length === 0) {
            return errorResponse(res, {
                message: "No user found"
            })
        }
        if (getUser[0].status !== "active") {
            return errorResponse(res, {
                message: "user is not active"
            })
        }
        let user = getUser[0];
        let matchHash = await compareHash(password, user.password)
        let token = await generateToken(user._id)
        return successResponse(res, {
            id: user._id,
            name: user.name,
            email: user.email,
            status: user.status,
            username: user.username,
            token: token,
            role: user.role
        });
    } catch (error) {
        console.log(error)
        return errorResponse(res, error)
    }
}

let create = async (req, res) => {
    let { email, name, username, mobile, role_id, password, status } = req.body
    console.log(req.body);

    try {
        let hash = await generateHash(password);
        console.log(hash)
        let user = await AdminUsers.create({
            name: name,
            email: email,
            password: hash,
            mobile: mobile,
            status: status,
            username: username,
            role_id: mongoose.Types.ObjectId(role_id)
        });

        return successResponse(res, {
            id: user._id,
            name: user.name,
            email: user.email,
            status: user.status,
            username: user.username
        });
    } catch (error) {
        return errorResponse(res, error)
    }
}

let updateStatus = async (req, res) => {
    let { id, status } = req.body
    try {
        let user = await AdminUsers.findByIdAndUpdate(id, {
            "$set": {
                status: status
            }
        })
        if (user == null) {
            return errorResponse(res, {
                message: "No user found"
            })
        }
        return successResponse(res, {
            id: user._id,
            name: user.name,
            email: user.email,
            status: user.status,
            username: user.username,
        });
    } catch (error) {
        console.log(error)
        return errorResponse(res, error)
    }
}

let createRole = async (req, res) => {
    let { name } = req.body
    try {
        let role = await Roles.create({
            name: name
        })
        return successResponse(res, role);
    } catch (error) {
        console.log(error)
        return errorResponse(res, error)
    }
}

let roleById = async (req, res) => {
    let { id } = req.params
    try {
        let role;
        if (id === "list") {
            role = await Roles.find({});
            return successResponse(res, role)
        }
        role = await Roles.findOne({
            name: id
        });
        if (role == null) {
            role = await Roles.findById(id);
        }
        return successResponse(res, role)
    } catch (error) {
        console.error(error)
        return errorResponse(res, error)
    }
}

module.exports = {
    login,
    create,
    updateStatus,
    createRole,
    roleById
} 