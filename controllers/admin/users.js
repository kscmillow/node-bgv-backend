let { errorResponse, successResponse } = require("../../utils/response");
const mongoose = require("mongoose")
let CompanyUsers = require("../../db/models/companyUsers");
let Roles = require("../../db/models/roles");
let { generateHash } = require("../../utils/bcrypt");

let adminList = async (req, res) => {
    try {
        let users = await AdminUsers.aggregate([
            {
                "$lookup": {
                    "from": "roles",
                    "localField": "role_id",
                    "foreignField": "_id",
                    "as": "role"
                }
            },
            {
                $project: {
                    _id: 1,
                    name: 1,
                    role: 1,
                    user_detail: 1,
                    status: 1,
                    username: 1
                }
            }
        ])
        return successResponse(res, users)
    } catch (error) {
        return errorResponse(res, error)
    }
}

let companyUserList = async (req, res) => {
    console.log(req.params)
    let aggregate = []
    if (req.params.id && req.params.id !== "list") {
        aggregate.push({
            "$match": {
                company_id: mongoose.Types.ObjectId(req.params.id)
            }
        })
    }
    aggregate.push({
        "$lookup": {
            "from": "roles",
            "localField": "role_id",
            "foreignField": "_id",
            "as": "role"
        }
    },
        {
            "$lookup": {
                "from": "companies",
                "localField": "company_id",
                "foreignField": "_id",
                "as": "company"
            }
        },
        {
            $project: {
                _id: 1,
                name: 1,
                role: 1,
                status: 1,
                username: 1,
                company: 1
            }
        })
    try {
        let users = await CompanyUsers.aggregate(aggregate)
        if(users && users.length > 0){
            return successResponse(res, users)
        }
        return successResponse(res, "no user found")
    } catch (error) {
        return errorResponse(res, error)
    }
}


let createCompanyUser = async (req, res) => {
    let { email, name, username, mobile, role_id, password, status, company_id } = req.body
    console.log(req.body);

    try {
        let hash = await generateHash(password);
        console.log(hash)
        let user = await CompanyUsers.create({
            name: name,
            email: email,
            password: hash,
            mobile: mobile,
            status: status,
            username: username,
            role_id: mongoose.Types.ObjectId(role_id),
            company_id: mongoose.Types.ObjectId(company_id)
        });

        return successResponse(res, {
            id: user._id,
            name: user.name,
            email: user.email,
            status: user.status,
            username: user.username
        });
    } catch (error) {
        return errorResponse(res, error)
    }
}


module.exports = {
    adminList,
    createCompanyUser,
    companyUserList
}